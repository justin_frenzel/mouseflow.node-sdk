var https = require('https');
var crypto = require("crypto");
var qs = require("querystring");
var fs = require("fs");

/*
@Mouseflow: Creates an API Wrapper for Mouseflow, useful for both public and trading calls to their API.
arguments: 
	options: {
		username: The email address associated with your Mouseflow account.
		apikey: the Api Key generated for your Mouseflow user account.
		datacenter: "us" or "eu", refers to your account data center. Defaults to US.
	}
*/
Mouseflow = function (options) {
	var self = this;

	if (typeof options !== "undefined") {
		if (options.username && options.apikey)
			self.authenticate(options.username, options.apikey);

		if (options.datacenter) {
			if (options.datacenter.toUpperCase() in self._datacenters)
				self.datacenter = self._datacenters[options.datacenter.toUpperCase()];
			else
				throw new Error("Error: '" + options.datacenter + "' is not a valid datacenter! Valid datacenters are [" + Object.keys(self._datacenters).join(",") + '].');
		}
		else
			self.datacenter = self._datacenters.US;
	}

	self.version = require('./package.json').version;
}

Mouseflow.prototype.auth = null;
Mouseflow.prototype.datacenter = null;

Mouseflow.prototype._datacenters = {
	US: "https://api-us.mouseflow.com/",
	EU: "https://api-eu.mouseflow.com/"
};

Mouseflow.prototype.authenticate = function authenticate (username, apikey) {
	if (username && apikey)
		this._auth = new Buffer(username.toLowerCase() + ":" + apikey).toString('base64');
	else
		throw new Error("Error: You must define a username and apikey to authenticate against the Mouseflow API with.");
};

Mouseflow.prototype._request = function _request (options, callback) {
	var self = this;

	options.hostname = options.url.split("https://")[1].split("/")[0];
	options.path = options.url.split(options.hostname)[1];
	options.port = null;

	var request = https.request(options, (response) => {
		var chunks = [];
		response.on("data", (chunk) => {
			chunks.push(chunk);
		});

		response.on("error", (err) => {
			callback(err, response, null);
		})

		response.on("end", () => {
			var body = Buffer.concat(chunks);
			callback(null, response, body.toString("utf-8"));
		});
	});

	if (options.body != null) {
		request.write(JSON.stringify(options.body));
		request.end();
	}
	else
		request.end();
}

Mouseflow.prototype._call = function (method, api, data, callback) {
	var self = this;

	if (!callback) {
		var callback = postData;
		postData = {};
	}

	var options = {
		url: self.datacenter + api,
		method: method,
		headers: {
			"User-Agent": "https://www.npmjs.com/package/mouseflow@" + self.version,
			"Content-Type": "application/json",
			"Accept": "application/json",
			"Authorization": (self._auth) ? "Basic " + self._auth : ""
		}
	};

	if (options.method == "GET" || options.method == "DELETE") {
		for (var i in data) {
			var token = options.url.indexOf("?") === -1 ? "?" : "&";
			options.url += token + i + "=" + encodeURIComponent(data[i]);
		}
	}
	else 
		options.body = data;

	var cb = function (error, response, body) {
		if (typeof callback == "function") {
			if (!error) {
				try {
					var data = JSON.parse(body);
				}
				catch (err) {
					var data = body;
				}

				if (response.statusCode < 300 && typeof data.error == "undefined")
					callback(null, response.statusCode == 204 ? true : data);
				else
					callback(data, null);
			}
			else
				callback(error, null);
		}
	}

	self._request(options, cb);
}

Mouseflow.prototype.get = function (endpoint, getData, callback) {
	if (typeof endpoint == "undefined")
		throw new Error("Mouseflow.get() requires an API endpoint (e.g. 'websites') as the first parameter!");
	this._call("GET", endpoint, typeof getData !== "function" ? getData : null, arguments[arguments.length-1]);
}

Mouseflow.prototype.post = function (endpoint, postData, callback) {
	if (typeof endpoint == "undefined")
		throw new Error("Mouseflow.post() requires an API endpoint (e.g. 'websites') as the first parameter!");
	this._call("POST", endpoint, typeof PostData !== "function" ? postData : null, arguments[arguments.length-1]);
}

Mouseflow.prototype.put = function (endpoint, putData, callback) {
	if (typeof endpoint == "undefined")
		throw new Error("Mouseflow.put() requires an API endpoint (e.g. 'websites') as the first parameter!");
	this._call("PUT", endpoint, typeof putData !== "function" ? putData : null, arguments[arguments.length-1]);
}

Mouseflow.prototype.delete = function (endpoint, getData, callback) {
	if (typeof endpoint == "undefined")
		throw new Error("Mouseflow.delete() requires an API endpoint (e.g. 'websites') as the first parameter!");
	this._call("DELETE", endpoint, typeof getData !== "function" ? getData : null, arguments[arguments.length-1]);
}


module.exports = Mouseflow;