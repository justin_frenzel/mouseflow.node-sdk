# Mouseflow Node.js Wrapper #

This is an **UNOFFICIAL** (unaffiliated with the company [Mouseflow](https://mouseflow.com/)) node.js package for communicating with Mouseflow REST API. https://api-docs.mouseflow.com/?javascript

This is currently just a simple wrapper, and doesn't expose the API as a traditional SDK (still a TODO, if there's demand).

## Installation ##

This driver is available as an NPM package: -

`npm install mouseflow`

Once installed, it can be used within your node.js project.

## How to Use ##

```js
const Mouseflow = require("mouseflow");
var ms = new Mouseflow({
	username: process.env.mouseflow_email || MY_MOUSEFLOW_EMAIL,
	apikey: process.env.mouseflow_apikey || MY_MOUSEFLOW_APIKEY,
	datacenter: process.end.mouseflow_datacenter || "US"
});
```

The username in this case is the email address tied to your Mouseflow account. Your API Key can be generated within the Mouseflow UI: https://us.mouseflow.com/settings/api. The property 'datacenter' is not required, and will default to "US"; you can point to either the "US" or "EU" data center.

## Making API Calls ##

This is a simple wrapper, so api methods aren't exposed directly as functions. Instead, there are four general methods available, "get", "post", "put" and "delete", which can be used to make API calls.

```js
const Mouseflow = require("mouseflow");
var ms = new Mouseflow({
	username: process.env.mouseflow_email || MY_MOUSEFLOW_EMAIL,
	apikey: process.env.mouseflow_apikey || MY_MOUSEFLOW_APIKEY,
	datacenter: process.end.mouseflow_datacenter || "US"
});

//Return your current websites list.
ms.get("websites", (err, websites) => {
	if (!err) { //'websites' will be an array of all your Mouseflow websites.
		console.log(data);
	}
	else //an error occurred.
		throw new Error(err);
		
});

//This will update the website with id '{website-id}' to "New Website Name".
ms.post("websites/{website-id}", {name: "New Website Name"}, (err, website) => {
	if (!err) { //'website' is the newly updated Mouseflow website object.
		console.log(data);
	}
	else //an error occurred, perhaps the id doesn't exist, or you aren't authenticated.
		throw new Error(err);
		
});
```

## MIT License ##
Copyright (c) 2017 Justin Frenzel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Support the Project ##

The git repository for this project is available on Bitbucket: https://bitbucket.org/justin_frenzel/mouseflow.node-sdk .

If you have any suggestions or would like to work on improving the project, feel free to submit a pull request or send me an email or something. Feel free to fork it if you'd like. If you run into any issues, please feel free to email me and I'll make bug fixes as needed.

If you want to make a donation, feel free to send BTC to this address: 1ENeXQyH25NQqgNWPHvXduh92k1TNFnCc9

## Author ##

* Justin Frenzel
* justin.frenzel@gmail.com